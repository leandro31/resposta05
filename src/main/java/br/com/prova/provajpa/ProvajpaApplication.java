package br.com.prova.provajpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class provajpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(provajpaApplication.class, args);
    }

}
